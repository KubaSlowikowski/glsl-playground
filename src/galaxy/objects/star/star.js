import * as THREE from "three";
import {getStarType, starsClassification} from "./starsClassification";
import {clamp, getRandomNumberFromRange} from "../../utils/utils";
import {StarConfig} from "../../configuration/starConfig";
import starImg from "../../images/star.png";
import {STAR_LAYER} from "../../configuration/postProcessingConfig";

export class Star {

    constructor(position) {
        this.position = position;

        this.starType = getStarType();
        this.starProperties = starsClassification[this.starType];

        this.radius = Math.pow(getRandomNumberFromRange(this.starProperties.sizeRange[0], this.starProperties.sizeRange[1], 2), StarConfig.SIZE_POW_RATIO) / StarConfig.SIZE_RATIO;
        this.color = this.starProperties.color;
        // this.luminosity = this.type.luminosity;
        this.obj = null;
    }

    toThreeObject(scene) {
        const geometry = new THREE.SphereGeometry(this.radius);
        // const material = new THREE.MeshBasicMaterial({ color: this.color });
        const material = materials[this.starType];

        const star = new THREE.Mesh(geometry, material);
        star.position.copy(this.position);
        star.layers.enable(STAR_LAYER);

        this.obj = star;
        scene.add(star);
    }

    updateScale(camera) {
        let dist = this.position.distanceTo(camera.position) / 75;

        // update star size
        let starSize = dist * this.radius;

        if (this.starType === 'SUPERGIANT_STAR' || this.starType === 'GIANT') { // giant stars must have smaller max size to make the galaxy look normal from the distance
            starSize = clamp(starSize, StarConfig.MIN_STAR_SIZE, StarConfig.MAX_STAR_SIZE / 12);
        } else {
            starSize = clamp(starSize, StarConfig.MIN_STAR_SIZE, StarConfig.MAX_STAR_SIZE);
        }

        this.obj?.scale.copy(new THREE.Vector3(starSize, starSize, starSize));
    }
}


const starTexture = new THREE.TextureLoader().load(starImg);
const materials = {
    O: new THREE.MeshBasicMaterial({ color: '#3986F2', map: starTexture}),
    SUPERGIANT_STAR: new THREE.MeshBasicMaterial({ color: '#ff2400', map: starTexture}),
    GIANT: new THREE.MeshBasicMaterial({ color: '#ff520d', map: starTexture}),
    B: new THREE.MeshBasicMaterial({ color: '#48A0EA', map: starTexture }),
    A: new THREE.MeshBasicMaterial({ color: '#8FECF1', map: starTexture }),
    F: new THREE.MeshBasicMaterial({ color: '#BCEFE0', map: starTexture }),
    G: new THREE.MeshBasicMaterial({ color: '#f1fd80', map: starTexture }),
    WHITE_DWARF: new THREE.MeshBasicMaterial({ color: '#ffffff', map: starTexture }),
    K: new THREE.MeshBasicMaterial({ color: '#FBDA33', map: starTexture }),
    M: new THREE.MeshBasicMaterial({ color: '#ff7513', map: starTexture }),
};
