export const starsClassification = {
    // main sequence stars
    O: { temperatureRange: [33000, 50000], sizeRange: [10,15], color: '#3986F2', abundance: 0.00001, luminosity: 100000 },
    SUPERGIANT_STAR: { temperatureRange: [4000, 40000], sizeRange: [30, 600], color: '#ff2400', abundance: 0.0001, luminosity: 1000000 },
    B: { temperatureRange: [10500, 33000], sizeRange: [5, 10], color: '#48A0EA', abundance: 0.1, luminosity: 1000 },
    GIANT: { temperatureRange: [3000, 10000], sizeRange: [10,50], color: '#ff520d', abundance: 0.4, luminosity: 100 },
    A: { temperatureRange: [7500, 10500], sizeRange: [1.7, 5], color: '#8FECF1', abundance: 0.7, luminosity: 20 },
    F: { temperatureRange: [6000, 7500], sizeRange: [1.3, 1.7], color: '#BCEFE0', abundance: 2, luminosity: 4 },
    G: { temperatureRange: [5500, 6000], sizeRange: [1, 1.3], color: '#f1fd80', abundance: 3.5, luminosity: 1 },
    WHITE_DWARF: { temperatureRange: [3000, 80000], sizeRange: [0.005, 0.01], color: '#ffffff', abundance: 5, luminosity: 0.01 },
    K: { temperatureRange: [4000, 5500], sizeRange: [0.8, 1], color: '#FBDA33', abundance: 8.29989, luminosity: 0.2 },
    M: { temperatureRange: [2600, 4000], sizeRange: [0.3, 0.8], color: '#ff7513', abundance: 80, luminosity: 0.01 },
};

export function getStarType() {
    // Calculate the total abundance
    let totalAbundance = 0;
    for (let key in starsClassification) {
        totalAbundance += starsClassification[key].abundance;
    }

    // Generate a random number between 0 and totalAbundance
    let random = Math.random() * totalAbundance;

    // Find the star that this random value falls into
    for (let key in starsClassification) {
        if (random < starsClassification[key].abundance) {
            return key;
        }
        random -= starsClassification[key].abundance;
    }

    // If no star is found (which should not happen), return null
    return 'M';
}
