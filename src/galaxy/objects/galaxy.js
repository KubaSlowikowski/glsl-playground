import * as THREE from "three";
import {
    INNER_CORE_X_DISTRIBUTION,
    INNER_CORE_Y_DISTRIBUTION,
    INNER_CORE_Z_DISTRIBUTION,
    NUM_ARMS, NUM_HAZE,
    NUM_STARS,
    OUTER_CORE_X_DISTRIBUTION,
    OUTER_CORE_Y_DISTRIBUTION,
    OUTER_CORE_Z_DISTRIBUTION
} from "../configuration/galaxyConfig";
import {gaussianRandom, logarithmicSpiral, Z_AXIS} from "../utils/utils";
import {Star} from "./star/star";
import {Haze} from "./haze/haze";


export class Galaxy {

    constructor(scene) {
        this.scene = scene;

        this.stars = this.createStars();
        this.haze = this.createHaze();

        this.stars.forEach(star => star.toThreeObject(this.scene));
        this.haze.forEach(haze => haze.toThreeObject(this.scene));
    }

    createStars() {
        const stars = [];

        createInnerCore();
        createOuterCore();
        createSpirals();

        return stars;


        function createInnerCore() {
            for (let i = 0; i < NUM_STARS / 3; i++) {
                const position = new THREE.Vector3(
                    gaussianRandom(0, INNER_CORE_X_DISTRIBUTION),
                    gaussianRandom(0, INNER_CORE_Y_DISTRIBUTION),
                    gaussianRandom(0, INNER_CORE_Z_DISTRIBUTION)
                );

                const star = new Star(position);
                stars.push(star);
            }
        }

        function createOuterCore() {
            for (let i = 0; i < NUM_STARS / 3; i++) {
                const position = new THREE.Vector3(
                    gaussianRandom(0, OUTER_CORE_X_DISTRIBUTION),
                    gaussianRandom(0, OUTER_CORE_Y_DISTRIBUTION),
                    gaussianRandom(0, OUTER_CORE_Z_DISTRIBUTION)
                );

                const star = new Star(position);
                stars.push(star);
            }
        }

        function createSpirals() {
            for (let rotation = 0; rotation <= 2 * Math.PI; rotation += 2 * Math.PI / NUM_ARMS) {
                for (let i = 0; i < NUM_STARS / NUM_ARMS / 3; i++) {
                    const position = logarithmicSpiral(i / 1500);
                    position.applyAxisAngle(Z_AXIS, rotation);

                    const star = new Star(position);
                    stars.push(star);
                }
            }
        }
    }

    createHaze() {
        const hazeArray = [];

        createInnerCore();
        createOuterCore();
        createSpirals();

        return hazeArray;

        function createInnerCore() {
            for (let i = 0; i < NUM_HAZE / 8; i++) {
                const position = new THREE.Vector3(
                    gaussianRandom(0, INNER_CORE_X_DISTRIBUTION),
                    gaussianRandom(0, INNER_CORE_Y_DISTRIBUTION),
                    gaussianRandom(0, INNER_CORE_Z_DISTRIBUTION)
                );

                const haze = new Haze(position);
                hazeArray.push(haze);
            }
        }

        function createOuterCore() {
            for (let i = 0; i < NUM_HAZE; i++) {
                const position = new THREE.Vector3(
                    gaussianRandom(0, OUTER_CORE_X_DISTRIBUTION),
                    gaussianRandom(0, OUTER_CORE_Y_DISTRIBUTION),
                    gaussianRandom(0, OUTER_CORE_Z_DISTRIBUTION)
                );

                const haze = new Haze(position);
                hazeArray.push(haze);
            }
        }

        function createSpirals() {
            for (let rotation = 0; rotation <= 2 * Math.PI; rotation += 2 * Math.PI / NUM_ARMS) {
                for (let i = 0; i < NUM_HAZE / NUM_ARMS; i++) {
                    const position = logarithmicSpiral(i / 2250);
                    position.applyAxisAngle(Z_AXIS, rotation);

                    const haze = new Haze(position);
                    hazeArray.push(haze);
                }
            }
        }
    }

    updateScale(camera) {
        this.stars.forEach(star => {
            star.updateScale(camera);
        });

        this.haze.forEach(haze => {
            haze.updateScale(camera);
        });
    }
}
