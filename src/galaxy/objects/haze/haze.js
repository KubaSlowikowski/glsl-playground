import * as THREE from "three";
import {BASE_LAYER} from "../../helpers/post-processing/postProcessing";
import {CENTER, clamp} from "../../utils/utils";
import {HazeConfig} from "../../configuration/hazeConfig";
import image from './hazeTexture.png';

const hazeTexture = new THREE.TextureLoader().load(image);
const hazeSpriteBlue = new THREE.SpriteMaterial({
    map: hazeTexture,
    color: 'rgba(151,204,255)',
    opacity: HazeConfig.OPACITY,
    depthTest: false,
    depthWrite: false
});
const hazeSpriteRed = new THREE.SpriteMaterial({
    map: hazeTexture,
    color: 'rgba(239,138,180)',
    opacity: HazeConfig.OPACITY,
    depthTest: false,
    depthWrite: false
});

export class Haze {

    constructor(position) {
        this.position = position;
        this.obj = null;
    }

    toThreeObject(scene) {
        const sprite = new THREE.Sprite(Math.random() > 0.4 ? hazeSpriteBlue : hazeSpriteRed);

        sprite.layers.set(BASE_LAYER);
        sprite.position.copy(this.position);

        sprite.scale.multiplyScalar(clamp(HazeConfig.MAX_SIZE * Math.random(), HazeConfig.MIN_SIZE, HazeConfig.MAX_SIZE));

        scene.add(sprite);
        this.obj = sprite;
    }

    updateScale(camera) {
        let dist = camera.position.distanceTo(CENTER) / 300;
        this.obj.material.opacity = clamp(HazeConfig.OPACITY * Math.pow(dist / 3, 2), 0, HazeConfig.OPACITY)
        this.obj.material.needsUpdate = true;
    }
}
