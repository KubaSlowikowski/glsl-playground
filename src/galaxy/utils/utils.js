import * as THREE from "three";
import {X_DISTRIBUTION, Y_DISTRIBUTION, Z_DISTRIBUTION} from "../configuration/galaxyConfig";

export const Z_AXIS = new THREE.Vector3(0, 0, 1);
export const CENTER = new THREE.Vector3(0,0,0);

export function logarithmicSpiral(rotationAngle) {
    // https://en.wikipedia.org/wiki/Logarithmic_spiral
    const a = 50;
    const phi = rotationAngle * (180 / Math.PI); // convert radians to degrees

    const alpha = 12; // in degrees
    const k = Math.tan(alpha * Math.PI / 180);

    let r = a * Math.exp(k * phi); // distance from the center
    let x = r * Math.cos(phi);
    let y = r * Math.sin(phi);

    x += gaussianRandom(0, X_DISTRIBUTION);
    y += gaussianRandom(0, Y_DISTRIBUTION);
    let z = gaussianRandom(0, Z_DISTRIBUTION);

    return new THREE.Vector3(x, y, z);
}

export function gaussianRandom(mean = 0, stdev = 1) {
    // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
    let u1 = 1 - Math.random();
    let u2 = Math.random();
    let z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);

    return z0 * stdev + mean;
}

/**
 * Returns the random number from the given range.
 * There's a higher possibility that random number will be closer to the min.
 */
export function getRandomNumberFromRange(min, max, precision) {
    var random = Math.random();
    random = Math.pow(random, 2); // Square the random number to skew the distribution
    var num = random * (max - min) + min;
    return parseFloat(num.toFixed(precision));
}

export function clamp(value, minimum, maximum) {
    return Math.min(maximum, Math.max(minimum, value))
}

export function showToast(message, orbit) {
    const toast = document.getElementById("toast");
    toast.textContent = message;
    toast.style.display = "block";

    document.addEventListener('click', () => {
        toast.style.display = "none";
        orbit.autoRotate = false;
    });
}
