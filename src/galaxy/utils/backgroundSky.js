import * as THREE from "three";

import bottom from '../images/sky.png';

export const sky = new THREE.CubeTextureLoader().load([bottom, bottom, bottom, bottom, bottom, bottom]);
