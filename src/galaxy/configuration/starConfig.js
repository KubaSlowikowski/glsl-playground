export const StarConfig = {
    MIN_STAR_SIZE: 0.05,
    MAX_STAR_SIZE: 15,
    SIZE_RATIO: 4,
    SIZE_POW_RATIO: 0.3
};

export const SUN_DIAMETER = 1_392_000;
export const EARTH_DIAMETER = 12_742;
