export const AXES_HELPER_ENABLED = false;
export const FOG = {
    enabled: true,
    near: 300,
    far: 10000,
};
