export const NUM_STARS = 2000;
export const NUM_HAZE = 1000;
export const NUM_ARMS = 4;

export const X_DISTRIBUTION = 3;
export const Y_DISTRIBUTION = 3;
export const Z_DISTRIBUTION = 2.5;

export const INNER_CORE_X_DISTRIBUTION = 25;
export const INNER_CORE_Y_DISTRIBUTION = 25;
export const INNER_CORE_Z_DISTRIBUTION = 4;

export const OUTER_CORE_X_DISTRIBUTION = 58;
export const OUTER_CORE_Y_DISTRIBUTION = 58;
export const OUTER_CORE_Z_DISTRIBUTION = 3.5;
