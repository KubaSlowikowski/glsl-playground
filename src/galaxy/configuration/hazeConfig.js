export const HazeConfig = {
    MIN_SIZE: 10,
    MAX_SIZE: 40,
    OPACITY: 0.2
};
