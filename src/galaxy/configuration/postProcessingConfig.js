import * as THREE from "three";

export const BLOOM_PARAMS = {
    strength: 1, // intensity of the effect
    radius: 0.9, // radius of the bloom
    threshold: 0.015 // minimum size to activate bloom
};

export const RENDERER_PARAMS = {
    toneMapping: THREE.CineonToneMapping,
    toneMappingExposure: 1,
    outputEncoding: THREE.sRGBEncoding,
};

export const BASE_LAYER = 0;
export const STAR_LAYER = 1;
