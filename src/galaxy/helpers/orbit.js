import {MapControls} from "three/examples/jsm/controls/MapControls";

export function initCameraControls(camera, domElement) {
    const orbit = new MapControls(camera, domElement);
    orbit.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
    orbit.dampingFactor = 0.05;
    orbit.screenSpacePanning = false;
    orbit.minDistance = 0.1;
    orbit.maxDistance = 100000;
    orbit.panSpeed = 0.5;
    orbit.rotateSpeed = 0.5;
    orbit.zoomSpeed = 0.5;
    orbit.autoRotateSpeed = 1.5;

    return orbit;
}
