import * as THREE from "three";

import vertexShader from './shaders/vertexShader.glsl';
import fragmentShader from './shaders/fragmentShader.glsl';

import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass';
import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer';
import {UnrealBloomPass} from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import {OutputPass} from 'three/examples/jsm/postprocessing/OutputPass';
import {ShaderPass} from 'three/examples/jsm/postprocessing/ShaderPass';
import {BLOOM_PARAMS, RENDERER_PARAMS, STAR_LAYER} from "../../configuration/postProcessingConfig";

export function initPostProcessing(renderer, scene, camera) {

    renderer.toneMapping = RENDERER_PARAMS.toneMapping;
    renderer.toneMappingExposure = RENDERER_PARAMS.toneMappingExposure;
    renderer.outputEncoding = RENDERER_PARAMS.outputEncoding;

    const renderScene = new RenderPass(scene, camera);

    const bloomPass = new UnrealBloomPass(
        new THREE.Vector2(window.innerWidth, window.innerHeight),
        BLOOM_PARAMS.strength,
        BLOOM_PARAMS.radius,
        BLOOM_PARAMS.threshold
    );

    const bloomComposer = new EffectComposer(renderer);
    bloomComposer.addPass(renderScene);
    bloomComposer.addPass(bloomPass);
    bloomComposer.renderToScreen = false;

    const mixPass = new ShaderPass(
        new THREE.ShaderMaterial({
            uniforms: {
                baseTexture: { value: null }, // contains original textures of the bloomed objects
                bloomTexture: { value: bloomComposer.renderTarget2.texture } // contains object textures after bloom effect is applied to them
            },
            vertexShader: vertexShader,
            fragmentShader: fragmentShader
        }), 'baseTexture'
    );
    // mixPass.needsSwap = true;

    const finalComposer = new EffectComposer(renderer);
    finalComposer.addPass(renderScene);
    finalComposer.addPass(mixPass);

    const outputPass = new OutputPass();
    finalComposer.addPass(outputPass);

    return { bloomComposer, finalComposer, bloomPass };
}

export const starLayer = new THREE.Layers();
starLayer.set(STAR_LAYER);
