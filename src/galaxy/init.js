import {initializeRenderer} from "./helpers/renderer";
import {SceneContext} from "./helpers/sceneContext";
import {CameraContext} from "./helpers/camera";
import {initPostProcessing} from "./helpers/post-processing/postProcessing";

export function initThree(animate) {
    // Renderer
    const renderer = initializeRenderer(animate);

    // Scene
    const sceneContext = new SceneContext();

    const { scene, stats } = sceneContext;

    // Stats
    document.body.appendChild(stats.dom);

    // Camera
    const { camera } = new CameraContext(renderer);

    return { renderer, scene, camera, stats };
}
