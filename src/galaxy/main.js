import * as THREE from "three";

import {initThree} from "./init";
import {Galaxy} from "./objects/galaxy";
import {initPostProcessing, starLayer} from "./helpers/post-processing/postProcessing";
import {BLOOM_PARAMS, STAR_LAYER} from "./configuration/postProcessingConfig";
import {EARTH_DIAMETER, StarConfig, SUN_DIAMETER} from "./configuration/starConfig";
import gsap from 'gsap';
import {showToast} from "./utils/utils";
import {initCameraControls} from "./helpers/orbit";
import * as dat from "dat.gui";


const { scene, renderer, camera, stats } = initThree(animate);
const { bloomComposer, finalComposer, bloomPass } = initPostProcessing(renderer, scene, camera);
const cameraControls = initCameraControls(camera, renderer.domElement);
addGui();
window.addEventListener('resize', onResize);


const galaxy = new Galaxy(scene);
addStarClickEffect();


function animate(time) {
    stats.update();
    cameraControls.update();
    galaxy.updateScale(camera);
    render();

    function render() {
        camera.layers.set(STAR_LAYER); // only stars visible
        bloomComposer.render(); // Create a bloom effect only from the stars.

        camera.layers.enableAll(); // Make everything visible on the scene.
        finalComposer.render(); // Render everything
    }
}


function onResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    bloomComposer.setSize(window.innerWidth, window.innerHeight);
    finalComposer.setSize(window.innerWidth, window.innerHeight);
}

function addGui() {
    const gui = new dat.GUI();
    gui.close();

    const bloomFolder = gui.addFolder('bloom');

    bloomFolder.add(BLOOM_PARAMS, 'strength').min(0).max(3).onChange(value => {
        bloomPass.strength = Number(value);
    });

    bloomFolder.add(BLOOM_PARAMS, 'radius').min(0).max(3).onChange(value => {
        bloomPass.radius = Number(value);
    });

    bloomFolder.add(BLOOM_PARAMS, 'threshold').min(0).max(1).onChange(value => {
        bloomPass.threshold = Number(value);
    });

    const starFolder = gui.addFolder('stars');

    starFolder.add(StarConfig, 'MAX_STAR_SIZE').min(0.01).max(100).onChange(value => {
        StarConfig.MAX_STAR_SIZE = Number(value);
    });
    starFolder.add(StarConfig, 'MIN_STAR_SIZE').min(0.01).max(5).onChange(value => {
        StarConfig.MIN_STAR_SIZE = Number(value);
    });
}

function addStarClickEffect() {
    const rayCaster = new THREE.Raycaster();
    const mouse = new THREE.Vector2();

    function onDblClick(event) {
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

        // Update the picking ray with the camera and mouse position
        rayCaster.setFromCamera(mouse, camera);

        // Get objects from a certain layer
        var objectsInLayer = scene.children.filter(object => starLayer.test(object.layers));
        const intersects = rayCaster.intersectObjects(objectsInLayer);

        if (intersects.length > 0) {
            const object = intersects[0].object;

            const radius = object.geometry.parameters.radius;
            const directionVector = new THREE.Vector3().subVectors(object.position, camera.position).normalize(); // direction from the camera to the object
            const targetPosition = new THREE.Vector3().addVectors(object.position, directionVector.multiplyScalar(-0.75 * radius));

            // Smoothly move camera to target position
            smoothCameraMovement(8);

            function smoothCameraMovement(durationInSeconds) {
                cameraControls.enabled = false;

                gsap.to(camera.position, {
                    x: targetPosition.x,
                    y: targetPosition.y,
                    z: targetPosition.z,
                    duration: durationInSeconds,
                    ease: 'power3.inOut',
                    onComplete: () => {
                        cameraControls.enabled = true;
                        cameraControls.autoRotate = true;
                        document.addEventListener('click', () => {
                            cameraControls.autoRotate = false;
                        });
                        createMessage();

                        function createMessage() {
                            const star = galaxy.stars.find(star => star.obj.uuid === object.uuid);
                            const sunRatio = Math.round(Math.pow(radius * StarConfig.SIZE_RATIO, 1 / StarConfig.SIZE_POW_RATIO) * 100) / 100;
                            const diameter = sunRatio * SUN_DIAMETER;
                            let message = `
                                This is a star of the '${star.starType}' class.
                                Its diameter is ${diameter.toLocaleString('de-DE')}km, which is ${sunRatio} times larger than our Sun.
                                You could line up ${Math.round(diameter / EARTH_DIAMETER).toLocaleString('de-DE')} Earths across the face of this star.
                                The surface temperature ranges from ${star.starProperties.temperatureRange[0]}K to ${star.starProperties.temperatureRange[1]}K.
                                Stars of this type make up ${star.starProperties.abundance}% of all the stars in the galaxy.
                            `;

                            showToast(message, cameraControls);
                        }
                    }
                });

                gsap.to(cameraControls.target, {
                    x: object.position.x,
                    y: object.position.y,
                    z: object.position.z,
                    duration: durationInSeconds / 2,
                    ease: 'power3.inOut',
                });
            }
        }

    }

    window.addEventListener('dblclick', onDblClick, false);
}
