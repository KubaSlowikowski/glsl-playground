import * as THREE from "three";
import {AXES_HELPER_ENABLED, FOG} from "../configuration/configuration";
import Stats from 'three/examples/jsm/libs/stats.module'
import {sky} from "../utils/backgroundSky";

export class SceneContext {

    constructor() {
        const scene = new THREE.Scene();
        this.scene = scene;

        scene.background = sky;

        if (FOG.enabled) {
            scene.fog = new THREE.FogExp2('#EBE2DB', 0.00003);
        }

        this.stats = new Stats();

        if (AXES_HELPER_ENABLED) {
            const axesHelper = new THREE.AxesHelper(5); //The X axis is red. The Y axis is green. The Z axis is blue.
            scene.add(axesHelper);
        }

        this.addLights();
    }

    addLights() {
        // ambient light which is for the whole scene
        const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
        this.scene.add(ambientLight);
    }
}
