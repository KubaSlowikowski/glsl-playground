import * as THREE from "three";

export function initializeRenderer(animationLoop) {
    const renderer = new THREE.WebGLRenderer({
        antialias: true,
    });
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setAnimationLoop(animationLoop);

    document.body.appendChild(renderer.domElement);
    return renderer;
}
