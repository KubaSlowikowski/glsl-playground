import * as THREE from "three";
import {CAMERA_CONFIG} from "../configuration/cameraConfig";

export class CameraContext {
  constructor(renderer) {
    const camera = new THREE.PerspectiveCamera(
      CAMERA_CONFIG.fov,
      window.innerWidth / window.innerHeight,
      CAMERA_CONFIG.near,
      CAMERA_CONFIG.far
    );
    camera.lookAt(0, 0, 0);
    camera.up.set(0, 0, 1);
    camera.position.set(CAMERA_CONFIG.position_x, CAMERA_CONFIG.position_y, CAMERA_CONFIG.position_z);

    this.camera = camera;
    this.renderer = renderer;
  }
}
