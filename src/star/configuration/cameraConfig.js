export const CAMERA_CONFIG = {
    position_x: 0,
    position_y: 3,
    position_z: 3,
    fov: 45,
    near: 0.1,
    far: 100000
};
