varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;

uniform float uTime;


void main() {
    vUv = uv;
    vPosition = position;
    vNormal = normal;

    vec4 result = vec4(position, 1.0);

    gl_Position = projectionMatrix * modelViewMatrix * result;
}
