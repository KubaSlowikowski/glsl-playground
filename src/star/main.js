import * as THREE from "three";
import {initThree} from "./init";
import {initCameraControls} from "./helpers/orbit";
import vertexShader from './sun/shaders/vertexShader.glsl';
import fragmentShader from './sun/shaders/fragmentShader.glsl';

function onResize(camera, renderer) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}


const { scene, renderer, camera, stats } = initThree(animate);
const cameraControls = initCameraControls(camera, renderer.domElement);
addGui();

const clock = new THREE.Clock();
const uniformData = {
    uTime: {
        type: 'f',
        value: clock.getElapsedTime()
    },
};

const sun = new THREE.Mesh(
    new THREE.SphereGeometry(1),
    new THREE.ShaderMaterial({
        vertexShader: vertexShader,
        fragmentShader: fragmentShader,
        uniforms: uniformData
    })
);
scene.add(sun);


window.addEventListener('resize', () => onResize(camera, renderer));

function animate(time) {
    renderer.render(scene, camera);
    stats.update();
    cameraControls.update();
    uniformData.uTime.value += clock.getDelta();
}

function addGui() {
    // const gui = new dat.GUI();
    // gui.close();
    //
    // const bloomFolder = gui.addFolder('bloom');
    //
    // bloomFolder.add(BLOOM_PARAMS, 'strength').min(0).max(3).onChange(value => {
    //     bloomPass.strength = Number(value);
    // });
}
