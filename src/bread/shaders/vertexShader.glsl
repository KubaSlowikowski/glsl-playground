out vec2 vUv;

uniform float u_time;

void main() {
    // projectionMatrix, modelViewMatrix, position -> passed in from Three.js
    vUv = uv;
    vec4 result;

    result = vec4(position, 1.0);

    gl_Position = projectionMatrix * modelViewMatrix * result;
}
