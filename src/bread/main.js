import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import vertexShader from "./shaders/vertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";


const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0x101114);

document.body.appendChild(renderer.domElement);

const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);

const controls = new OrbitControls(camera, renderer.domElement);

//controls.update() must be called after any manual changes to the camera's transform
camera.position.set(0,0,1.25);
controls.update();

// const axesHelper = new THREE.AxesHelper( 5 );
// scene.add( axesHelper );

const clock = new THREE.Clock();

const uniformData = {
    uTime: {
        type: 'f',
        value: clock.getElapsedTime()
    },
};

const geometry = new THREE.PlaneGeometry(1,1);

const material = new THREE.ShaderMaterial({
    // wireframe: true,
    uniforms: uniformData,
    vertexShader: vertexShader,
    fragmentShader: fragmentShader
});
const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    uniformData.uTime.value += clock.getDelta();
}

animate();
