import * as THREE from 'three'
import {addPass, useCamera, useGui, useRenderSize, useScene, useTick} from './render/init.js'
// import postprocessing passes
import {SavePass} from 'three/examples/jsm/postprocessing/SavePass.js'
import {ShaderPass} from 'three/examples/jsm/postprocessing/ShaderPass.js'
import {CopyShader} from 'three/examples/jsm/shaders/CopyShader.js'
import vertexShader from "./shaders/vertexShader.glsl";
import fragmentShader from "./shaders/fragmentShader.glsl";
import {DoubleSide} from "three";
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';

const startApp = () => {
    const scene = useScene()
    const camera = useCamera()
    const gui = useGui()
    const { width, height } = useRenderSize()

    // settings
    const MOTION_BLUR_AMOUNT = 0.725

    // lighting
    const dirLight = new THREE.DirectionalLight('#ffffff', 0.75)
    dirLight.position.set(5, 5, 5)

    const ambientLight = new THREE.AmbientLight('#ffffff', 0.2)
    scene.add(dirLight, ambientLight)


    // meshes
    const torus = new THREE.Mesh(
        new THREE.TorusGeometry(1, 0.3, 1000, 1000),
        new THREE.ShaderMaterial({
            vertexShader,
            fragmentShader,
            side: DoubleSide,
            // wireframe: true,
            uniforms: {
                uTime: { value: 0}
            }
        })
    );
    scene.add(torus);

    // GUI
    const cameraFolder = gui.addFolder('Camera')
    cameraFolder.add(camera.position, 'z', 0, 10)
    cameraFolder.open()

    // postprocessing
    const renderTargetParameters = {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.LinearFilter,
        stencilBuffer: false,
    }

    // save pass
    const savePass = new SavePass(new THREE.WebGLRenderTarget(width, height, renderTargetParameters))

    // blend pass
    // const blendPass = new ShaderPass(BlendShader, 'tDiffuse1')
    // blendPass.uniforms['tDiffuse2'].value = savePass.renderTarget.texture
    // blendPass.uniforms['mixRatio'].value = MOTION_BLUR_AMOUNT

    // output pass
    const outputPass = new ShaderPass(CopyShader)
    outputPass.renderToScreen = true

    const bloomPass = new UnrealBloomPass(
        new THREE.Vector2(window.innerWidth, window.innerHeight),
        1.4,
        0.001,
        0.01
    );

    // adding passes to composer
    // addPass(blendPass)
    addPass(savePass)
    addPass(outputPass)
    addPass(bloomPass)

    useTick(({ timestamp, timeDiff }) => {
        const time = timestamp / 1000;
        torus.material.uniforms.uTime.value = time;
    })
}

export default startApp
