uniform float uTime;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;

#define PI 3.141592

// cosine based palette, 4 vec3 params
vec3 palette(float t) { // https://iquilezles.org/articles/palettes/
    vec3 a = vec3(0.5, 0.5, 0.5);
    vec3 b = vec3(0.5, 0.5, 0.5);
    vec3 c = vec3(1.0, 1.0, 1.0);
    vec3 d = vec3(0.263, 0.416, 0.557);

    return a + b * cos(2.0 * PI * (c * t + d));
}

void main() {
    vec2 uv = (vUv - 0.5) * 2.0;

    vec2 uv0 = uv;

    vec3 finalColor = vec3(0.0, 0.0, 0.0);

    for (float i = 0.0; i < 3.0; i++) {
        uv *= 1.5;
        uv = fract(uv);
        uv -= 0.5;

        float d = length(uv) * exp(-length(uv0));
//        float d = length(uv);

        vec3 col = palette(length(uv0) + i*0.4 + uTime / 2.0);
//        vec3 col = palette(length(uv0) + uTime / 2.0);

        d = sin(d * 10.0 + uTime) / 10.0;
        d = abs(d);

        d = 0.01 / d;
        d = pow(d, 1.1);

        finalColor += col * d;
    }

    gl_FragColor = vec4(finalColor, 1.0);

}
