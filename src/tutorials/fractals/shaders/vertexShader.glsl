uniform float uTime;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;

void main() {
    // Transform -> position, scale, rotation
    // modelMatrix -> position, scale, rotation of our model
    // viewMatrix -> position, orientation of our camera
    // projectionMatrix -> projects our object onto the screen (aspect ratio & the perspective)

    vPosition = position;
    vNormal = normal;
    vUv = uv;

    vec4 modelViewPosition = modelMatrix * viewMatrix * vec4(vPosition, 1.0);
    vec4 projectedPosition = projectionMatrix * modelViewPosition;

    gl_Position = projectedPosition;
}
